import { Token } from 'vue-typedi'

export default {
  // Axios instance
  AXIOS: new Token('axios')
}
